# GetGoing Startpage

A basic, minimal start-page for Firefox

![Alt text](GetGoingStartpage.gif)



## Getting started

A simple html startpage for Firefox designed for use with with web-based pixelart, gifs and web images. 

![Alt text](https://c.tenor.com/59F2fWU66e4AAAAC/confidence-beach.gif "confidence-beach gif used in this startpage")

**confidence-beach gif used in this startpage**

## Origins

This start-page was developed with the help of [Mariusz Z](https://gitlab.com/vallode). [This guide](https://stpg.tk/guides/basic-startpage/) enabled me to build my first start-page from scratch (almost). The goal is to keep the design simple, useful and humorous.

## Installation
For the startpage: Enable the new tab startpage functionality open Firefox settings and then go to the `home` tab. Under `New Windows and Tabs` select `Custom URLs`  to change your home tab to the GetGoing.html document to serve as the browser start-page. = "file:///home/£YOUR-USER-NAME£/GetGoing.html"

## Editing

### Message and Links
To edit the html document, open it with a text editor. Look in the `body `section of the document, at the bottom, it looks like this...

```
<body>
  <nav>
  <h1>Welcome Back Bruv!</h1>
  <ul>
    <li>Get Going</li>
    <li><a href="https://twitter.com/home">twitter</a><li>
    <li><a href="https://www.youtube.com/">YouTube</a><li>
    <li><a href="https://www.reddit.com/">reddit</a><li>
      </ul> 
 </nav>
```
Change the welcome message by replacing `<h1>Welcome Back Bruv!</h1>` with `<h1>Any message you want</h1>`

Change the links..

```
 <li>Get Going</li>
    <li><a href="https://twitter.com/home">twitter</a><li>
    <li><a href="https://www.youtube.com/">YouTube</a><li>
    <li><a href="https://www.reddit.com/">reddit</a><li>
```
by replacing the links you want. Don't forget to change the title of the link at the end of the line.

### Background Colour and Font 
To change the Background colour look in the `style` section of the document at the top. It looks like this...

```
<head>
  <title>New tab</title>
 <style>
    html {
      align-items: center;
      background-color: #91a3b0 ;
      color: #313131;
      display: flex;
      font: 22px "Courier Prime", Courier, monospace;
      height: 100%;
      justify-content: center;
      margin: 0;
```

Use a [html color code](https://html-color.codes/) to change the background colour.

Use a monospace web font to change the font, [here are a few](https://fonts.google.com/?category=Monospace)


## Support
The origional author of this document is [Mariusz Z](https://gitlab.com/vallode). [His guide](https://stpg.tk/guides/basic-startpage/) got me started making this doc. [The reddit startpage community is also very helpful](https://www.reddit.com/r/startpages/)

## Roadmap
I would like to learn how to add multiple links and clocks and weather reports


## Authors and acknowledgment
A big thank you to [Mariusz Z](https://gitlab.com/vallode)

## License
MIT license.

## Project status
Development is slow due to life.

